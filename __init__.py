import os
import sys

import mathutils
import bpy

from skl import Skl

#main
#must to set SKL file's full path
#filename = r"C:\FZERONexus\GXmod\Rom_Data\SMB2_JP\ape\aiai_face.skl"
filename = r"C:\FZERONexus\GXmod\Rom_Data\SMB2_JP\ape\aiai.skl"

file = open(filename, 'rb')

skl = Skl()
skl.read(file)

if bpy.context.mode == 'EDIT_ARMATURE':
    bpy.ops.object.mode_set(mode='OBJECT', toggle=False)
    
#Add Armature
bpy.ops.object.add(type='ARMATURE', location = (0, 0 ,0), enter_editmode=False)

for i in range(len(skl.joints)):
    bone = skl.joints[i]
    parentID = bone.parentID
    entryName = bone.name
    q1 = bone.rotation1
    q2 = bone.rotation2
    pos3D = bone.position
    
    #Edit Mode
    bpy.ops.object.mode_set(mode='EDIT')
    #Instatiate Bone
    b = bpy.context.object.data.edit_bones.new(name = entryName)
    
    head = mathutils.Vector((0, 0, 0))
    tail = mathutils.Vector((0, 0, 0))
    if parentID < 0:
        head = mathutils.Vector((0, -0.1, 0))
        tail = mathutils.Vector((0, 0, 0))
    else:
        b.parent = bpy.context.object.data.edit_bones[parentID]
        head = mathutils.Vector(bpy.context.object.data.edit_bones[parentID].tail)
        tail = q2 * mathutils.Vector((0, 0, 0)) + pos3D + head
    
    b.head = head
    b.tail = tail
    
#Object Mode
bpy.ops.object.mode_set(mode='OBJECT', toggle=False)