import os
import sys

#read Name
def get_name(file):
    nameOffset = 0
    offset = file.tell()
    entryName = ''
    while (True):
        file.seek(offset + nameOffset)
        rawBin = file.read(1)
        charName = rawBin.decode('UTF-8')

        if rawBin == b'\x00':
            file.seek(file.tell()-1)
            return entryName
        else:
            entryName += str(charName)
            nameOffset = nameOffset + 1
