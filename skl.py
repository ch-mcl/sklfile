import os
import sys

import numpy
import mathutils

import av_util as util

#SKL file structure
class Skl:
    def __init__(self):
        self.count = 0
        self.joints = []
        
    def read(self, file):
        self.count = numpy.fromfile(file, '>i2', 1)
        #skip paddings
        file.seek(file.tell()+2)
        print('Entry:')
        print(self.count)

        #Read Entry Datas
        for i in range(self.count):
            print('---')
            joint = Joint()
            joint.read(file)
            self.joints.append(joint)
            print('parent:\t'+str(joint.parentID))
            print('filePOS:'+str(file.tell()) + ':\t' + str(joint.name))
            
            fpos = file.tell()
            if(fpos%4 == 0):
                fpos = fpos + 4
            while(fpos%4 != 0):
                fpos = fpos + 1
            file.seek(fpos)

class Joint:
    def __init__(self):
        self.parentID = 0x00
        self.flag = 0x00
        self.rotation1 = mathutils.Quaternion((1, 0, 0, 0))
        self.rotation2 = mathutils.Quaternion((1, 0, 0, 0))
        self.position = mathutils.Vector((0, 0, 0))
        self.name = ''
        
    def read(self, file): 
        self.parentID = numpy.fromfile(file, '>i2', 1)
        self.flag = numpy.fromfile(file, '>i2', 1)
        #get Rotation1
        rotX1 = numpy.fromfile(file, '>f4', 1)
        rotY1 = numpy.fromfile(file, '>f4', 1)
        rotZ1 = numpy.fromfile(file, '>f4', 1)
        rotW1 = numpy.fromfile(file, '>f4', 1)
        self.rotation1 = mathutils.Quaternion((rotW1, rotX1, rotY1, rotZ1))
        #get Rotation2
        rotX2 = numpy.fromfile(file, '>f4', 1)
        rotY2 = numpy.fromfile(file, '>f4', 1)
        rotZ2 = numpy.fromfile(file, '>f4', 1)
        rotW2 = numpy.fromfile(file, '>f4', 1)
        self.rotation2 = mathutils.Quaternion((rotW2, rotX2, rotY2, rotZ2))
        #get position
        posX = numpy.fromfile(file, '>f4', 1)
        posY = numpy.fromfile(file, '>f4', 1)
        posZ = numpy.fromfile(file, '>f4', 1)
        self.position = mathutils.Vector((posX, posY, posZ))
        #get Name
        self.name = util.get_name(file)